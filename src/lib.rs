extern crate byteorder;
extern crate hex;

use byteorder::{ByteOrder, BigEndian};

pub fn sha256(data: &[u8]) -> Box<[u8; 32]> {
    let sha256 = Sha256::new();
    sha256.digest(data)
}

trait Sha2 {
    type Block;
}
pub struct Sha256 {
    h: [u32; 8]
}
impl Sha2 for Sha256 {
    type Block = [u8; 64];
}
impl Sha256 {
    const K: [u32; 64] = [
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
        0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
        0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
        0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
        0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
        0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
        0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
        0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
        0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    ];

    pub fn new() -> Sha256 {
        Sha256 {
            h: [ 0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
                 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19 ]
         }
    }

    pub fn digest(mut self, data: &[u8]) -> Box<[u8; 32]> {
        // TODO: Don't copy the entire vector. Only copy the last block which needs to be padded
        let mut padded = Vec::with_capacity(data.len() + (64 - data.len() % 64));
        padded.extend_from_slice(data);

        let message_len = { padded.len() * 8 };
        Self::pad_message(&mut padded, message_len);

        for block in padded.chunks(64) {
            let block: &[u8; 64] = unsafe { std::mem::transmute(block.as_ptr()) };
            self.process_block(block);
        }

        let result = unsafe {
            let mut result: Box<[u8; 32]> = Box::new(std::mem::uninitialized());
            self.h.iter().enumerate().for_each(
                |(i, h)| BigEndian::write_u32(&mut result[i*4..i*4+4], *h)
            );
            result
        };

        result
    }

    fn process_block(&mut self, block: &<Self as Sha2>::Block) {
        let m = Self::init_m(block);
        let w = unsafe {
            let mut w: [u32; 64] = std::mem::uninitialized();
            for t in 0..16 {
                w[t] = m[t];
            }
            for t in 16..64 {
                w[t] = Self::ssig1(w[t-2])
                    .wrapping_add(w[t-7])
                    .wrapping_add(Self::ssig0(w[t-15]))
                    .wrapping_add(w[t-16]);
            }
            w
        };

        let [mut a, mut b, mut c, mut d, mut e, mut f, mut g, mut h] = self.h;
        for t in 0..64 {
            let t1 = h
                    .wrapping_add(Self::bsig1(e))
                    .wrapping_add(Self::ch(e, f, g))
                    .wrapping_add(Self::K[t])
                    .wrapping_add(w[t]);
            let t2 = Self::bsig0(a)
                    .wrapping_add(Self::maj(a, b, c));
            h = g;
            g = f;
            f = e;
            e = d.wrapping_add(t1);
            d = c;
            c = b;
            b = a;
            a = t1.wrapping_add(t2);
        }
        self.h[0] = a.wrapping_add(self.h[0]);
        self.h[1] = b.wrapping_add(self.h[1]);
        self.h[2] = c.wrapping_add(self.h[2]);
        self.h[3] = d.wrapping_add(self.h[3]);
        self.h[4] = e.wrapping_add(self.h[4]);
        self.h[5] = f.wrapping_add(self.h[5]);
        self.h[6] = g.wrapping_add(self.h[6]);
        self.h[7] = h.wrapping_add(self.h[7]);
    }

    /// Pads the message for hashing. If the message length is not a multiple of 8 **bits**
    /// then the trailing bits must be filled with zeroes.
    fn pad_message(data: &mut Vec<u8>, bit_len: usize) {
        let bits_to_fill = 8 - (bit_len % 8);
        let bytes_to_append = ((448 - bit_len % 512) / 8) - bits_to_fill / 8;
        let new_capacity = { data.len() + bytes_to_append };
        data.reserve_exact(new_capacity);
        match bits_to_fill {
            8 => data.push(128),
            n => {
                let last = data.last_mut().unwrap();
                *last = *last | 1u8 << (n - 1);
            }
        };
        (0..bytes_to_append).for_each(|_| data.push(0));
        let mut len = [0u8; 8];
        BigEndian::write_u64(&mut len, bit_len as u64);
        len.iter_mut().for_each(|b| data.push(*b));
    }

    fn init_m(block: &<Self as Sha2>::Block) -> [u32; 16] {
        unsafe {
            let mut m: [u32; 16] = std::mem::uninitialized();
            block.chunks_exact(4).enumerate().map(
                |(i, chunk)| m[i] = BigEndian::read_u32(&chunk)
            ).count();
            m
        }
    }

    fn ch(x: u32, y: u32, z: u32) -> u32  { (x & y) ^ ( !x & z) }
    fn maj(x: u32, y: u32, z: u32) -> u32 { (x & y) ^ (x & z) ^ (y & z) }

    fn bsig0(x: u32) -> u32 {
        x.rotate_right(2) ^ x.rotate_right(13) ^ x.rotate_right(22)
    }

    fn bsig1(x: u32) -> u32 {
        x.rotate_right(6) ^ x.rotate_right(11) ^ x.rotate_right(25)
    }

    fn ssig0(x: u32) -> u32 {
        x.rotate_right(7) ^ x.rotate_right(18) ^ (x >> 3)
    }

    fn ssig1(x: u32) -> u32 {
        x.rotate_right(17) ^ x.rotate_right(19) ^ (x >> 10)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test] fn pad_empty_message() {
        let mut data = vec![];
        Sha256::pad_message(&mut data, 0);
        let mut expected = vec![0; 64];
        expected[0] = 128;
        assert_eq!(expected, data);
    }
    #[test] fn pad_byte_message() {
        let mut data = vec![97, 98, 99, 100, 101];
        let len = { data.len() * 8 };
        Sha256::pad_message(&mut data, len);
        let mut expected = vec![0u8; 64];
        expected[0] = 97;
        expected[1] = 98;
        expected[2] = 99;
        expected[3] = 100;
        expected[4] = 101;
        expected[5] = 128;
        expected[63] = 40;
        assert_eq!(expected, data);
    }

    #[test] fn pad_bit_message_1() {
        let mut data = vec![97, 98, 99, 100, 100];
        Sha256::pad_message(&mut data, 39);
        let mut expected = vec![0u8; 64];
        expected[0] = 97;
        expected[1] = 98;
        expected[2] = 99;
        expected[3] = 100;
        expected[4] = 101;
        expected[63] = 39;
        assert_eq!(expected, data);
    }

    #[test] fn pad_bit_message_2() {
        let mut data = vec![97, 98, 99, 100, 128];
        Sha256::pad_message(&mut data, 33);
        let mut expected = vec![0u8; 64];
        expected[0] = 97;
        expected[1] = 98;
        expected[2] = 99;
        expected[3] = 100;
        expected[4] = 192;
        expected[63] = 33;
        assert_eq!(expected, data);
    }

    #[test] fn empty_message_struct() {
        let sha256 = Sha256::new();
        let hash = sha256.digest(b"");

        let expected = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
        assert_eq!(expected, hex::encode(hash.to_vec()));
    }

    #[test] fn short_message_struct() {
        let sha256 = Sha256::new();
        let hash = sha256.digest(b"The quick brown fox jumps over the lazy dog");

        let expected = hex::decode(
            "d7a8fbb307d7809469ca9abcb0082e4f8d5651e46d3cdb762d02d0bf37c9e592"
        ).unwrap();
        assert_eq!(expected.to_vec(), hash.to_vec());
    }

    #[test] fn short_message_2() {
        let hash = sha256(b"The quick brown fox jumps over the lazy dog.");

        let expected = "ef537f25c895bfa782526529a9b63d97aa631564d5d789c2b765448c8635fb6c";
        assert_eq!(expected, hex::encode(hash.to_vec()));
    }

    #[test] fn ch_trivial() {
        assert_eq!(0xffffffff, Sha256::ch(0xffffffff, 0xffffffff, 0xffffffff));
        assert_eq!(0x00000000, Sha256::ch(0x00000000, 0x00000000, 0x00000000));
        assert_eq!(0xffffffff, Sha256::ch(0x00000000, 0x00000000, 0xffffffff));
        assert_eq!(0xffffffff, Sha256::ch(0xffffffff, 0xffffffff, 0x00000000));
    }

    #[test] fn ch_2() {
        assert_eq!(0x000f000f, Sha256::ch(0xff00ff00, 0x00ff00ff, 0x0f0f0f0f));
        assert_eq!(0x57df9a19, Sha256::ch(0x12345678, 0xfedcba98, 0x55ff8819));
    }

    #[test] fn maj_trivial() {
        assert_eq!(0xffffffff, Sha256::maj(0xffffffff, 0xffffffff, 0xffffffff));
        assert_eq!(0x00000000, Sha256::maj(0x00000000, 0x00000000, 0x00000000));
        assert_eq!(0x00000000, Sha256::maj(0x00000000, 0x00000000, 0xffffffff));
        assert_eq!(0xffffffff, Sha256::maj(0xffffffff, 0xffffffff, 0x00000000));
    }

    #[test] fn bsig0_trivial() {
        assert_eq!(0xffffffff, Sha256::bsig0(0xffffffff));
        assert_eq!(0x00000000, Sha256::bsig0(0x00000000));
    }

    #[test] fn bsig1_trivial() {
        assert_eq!(0xffffffff, Sha256::bsig1(0xffffffff));
        assert_eq!(0x00000000, Sha256::bsig1(0x00000000));
    }

    #[test] fn ssig0_trivial() {
        assert_eq!(0x1fffffff, Sha256::ssig0(0xffffffff));
        assert_eq!(0x00000000, Sha256::ssig0(0x00000000));
    }

    #[test] fn ssig1_trivial() {
        assert_eq!(0x003fffff, Sha256::ssig1(0xffffffff));
        assert_eq!(0x00000000, Sha256::ssig1(0x00000000));
    }

    #[test] fn init_m() {
        assert_eq!([0; 16], Sha256::init_m(&[0; 64]));
        assert_eq!([0x01010101; 16], Sha256::init_m(&[0x01; 64]));
        assert_eq!([0x11111111; 16], Sha256::init_m(&[0x11; 64]));
    }
}
