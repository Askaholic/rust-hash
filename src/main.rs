extern crate rust_hash;
extern crate hex;

use std::time::{Instant};

fn main() {
    const SIZE: usize = 100_000;
    let mut data = Vec::with_capacity(SIZE);
    data.resize(SIZE, 0);

    // let mut hash = Box::new([0u8; 32]);
    // let start = Instant::now();
    // for _ in 0..5 {
    let hash = rust_hash::sha256(&data);
    // }
    // let duration = start.elapsed();
    //
    // println!("Hash: {} took: {:?}", hex::encode(hash.to_vec()), duration / 50);
}
